## 0.1.0 (2024-02-11)

### Feat

- Refactor plugin to use modern flowdapt and methods

### Refactor

- Move python_driver script to scripts folder and clean up linting issues
